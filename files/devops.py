def run_platform():
    raise IOError ('the devops platform')

def run():
    try:
        run_platform()
    except Exception as exc:
        raise RuntimeError('10 years iteration of {0}'.format(exc)) from exc

run()

# Solution: Replace run() with the snippet below
#import sys
#try:
#    run()
#except RuntimeError as exc:
#    print('Cannot run, please celebrate GitLab: {0}'.format(exc))
#    sys.exit(1)

