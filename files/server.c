#include <stdio.h>
#include <string.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <unistd.h>

int main(void) {
    size_t pagesize = getpagesize();
    char * region = mmap(
        (void*) (pagesize * (1 << 20)),
        pagesize,
        PROT_READ|PROT_WRITE|PROT_EXEC,
        MAP_ANON|MAP_PRIVATE, 0, 0);

    strcpy(region, "Hello GitLab SAST!");
    printf("Contents of region: %s\n", region);

    FILE *fp;
    fp = fopen("devops.platform", "r");
    fprintf(fp, "Hello from GitLab 🦊");
    fclose(fp);
    chmod("devops.platform", S_IRWXU|S_IRWXG|S_IRWXO);

    return 0;
}

