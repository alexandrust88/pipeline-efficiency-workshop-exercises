# Standalone projects for exercises

## cicd-tanuki-cpp

Sources are used to build a raw C++20 project.

Dockerfile comes into play for optimizations. 

CI/CD configuration is part of the exercises, `tanuki_cpp_*` prefix. 
